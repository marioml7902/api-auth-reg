'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');
const SECRET = require('../config').secret;
const EXP_TIME = require('../config').tokenExpTmp;

// crearToken
// devuelve un token de tipo JWT
// formato jwt:
// HEADER.PAYLOAD.VERIFY_SIGNATURE
//
// header: objeto json con el algoritmo codificado en base url 64
//      {
//			"alg": "HS256"	
//			"typ": "JWT"
//		}
//
// payload: contiene los claims
//		{
//		"sub":"1234567890"
//		"name": "laura escribano"
//		"iat": moment().unix()
// 		}
//
// VERIFY_SIGNTURE: HMACSHA256( base64UrlEncode(PAYLOAD)+"."+base64UrlEncode(PAYLOAD), SECRET )


function creaToken(user){
	const payload = {
		sub: user._id,
		iat: moment().unix(),
		exp: moment().add(EXP_TIME, 'minutes').unix()
	};
	return jwt.encode(payload, SECRET);
}

function decodificaToken(token){
	return new Promise((resolve, reject) => {
		try{
			const payload = jwt.decode(token, SECRET, true);
			if(payload.exp <= moment().unix()){
				reject({
					status: 401,
					message: 'El token ha caducado'
				});
				resolve(payload.sub);
			}
			console.log(payload);
			resolve(payload.sub);
		}		
		catch{
			reject({
				status: 500,
				message: 'El token no es válido'
			});
		}
	});
}

module.exports = {
	creaToken,
	decodificaToken
};
